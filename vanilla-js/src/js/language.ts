import languageData from '../data/labels.json'

let languageActive = 'en-US';

const changesLanguage = () => {
    // @ts-ignore
    let currentLanguage = languageData[languageActive]

    document.getElementById('title')!.innerText = currentLanguage.title

    document.getElementById('button-add-to-do')!.innerText = currentLanguage.buttonLabel;


    (document.getElementById('input-to-do') as HTMLInputElement).placeholder = currentLanguage.inputText;

    const buttonFlag = document.getElementById('button-change-language')!;
    buttonFlag.setAttribute('data-language', languageActive);
    buttonFlag.innerText = currentLanguage.buttonFlag
}

changesLanguage()

document.getElementById('button-change-language')!.addEventListener('click', (e) => {
    if (languageActive === 'en-US') {
        languageActive = 'es-MX'
    } else {
        languageActive = 'en-US'
    }
    changesLanguage()
});
