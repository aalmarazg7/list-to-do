import type { ToDo } from '../types';
import { v4 as uuidv4 } from 'uuid';
import { loadToDo, createToDo, upDateToDo, deleteToDo } from './data-sourse';
import { compareAsc } from "date-fns";

let toDoList: ToDo[] = []

let newToDo = '';

const displayToDo = () => {
    let listHtml = '';
    //ordenamos los toDO list por fecha
    toDoList = toDoList.sort((a, b) => compareAsc(a.creationDate, b.creationDate));

    //recorremos la todolist para crear elementos dentro de la ul
    for (let i = 0; toDoList.length > i; i++) {

        //cramos un nuevo rubro para editar elc ontenido del input 
        if (toDoList[i].isEditing) {
            listHtml = listHtml.concat(`<li>
            <input type="text" name="input" id="input-to-do-edit-${i}" value ="${toDoList[i].value}">
            <button type="button" class="button-edit-save-to-do" data-item-list="${i}">save</button>
            <button type="button" class="button-edit-cancel-to-do" data-item-list="${i}">cancel</button>
            <p class = "error-mesage-input-edit-toDo" data-item-error="${i}"> **Este campo no debe de ir vacio...</p>
        </li>`)
        } else if (toDoList[i].isComplete) {
            listHtml = listHtml.concat(`<li>
            <button type= "button" class = "button-no-complete-to-do" data-item-list="${i}">No Complete</button>
            ${toDoList[i].value}
            <button type="button" class="button-delete-to-do" data-item-list="${i}">delete</button>
            </li>`)
        } else {
            listHtml = listHtml.concat(`<li>
            <button type= "button" class = "button-complete-to-do" data-item-list="${i}">Complete</button>
            ${toDoList[i].value}
            <button type= "button" class = "button-edit-to-do" data-item-list="${i}">edit</button>
           
        </li>`)
        };


    };
    //cada elemento se va a agregar en la UL y encuanto se agregue se limpa la memoria
    const ulToDoList = document.getElementById('to-do-list-ul')!;
    while (ulToDoList.lastElementChild) {
        ulToDoList.removeChild(ulToDoList.lastElementChild)
    }
    ulToDoList.innerHTML = listHtml

    //se agrego al boton delete y se le dio funcionalidad al momento de dar click al elemento a borrar
    const buttonsDelete = document.querySelectorAll('.button-delete-to-do')!;
    for (let i = 0; buttonsDelete.length > i; i++) {
        buttonsDelete[i].addEventListener('click', (e) => {
            const toDoIndex = parseInt((e.target! as HTMLElement).dataset.itemList!)
            deleteToDo(toDoList[toDoIndex], (error?: Error) => {
                if (!error) {
                    toDoList = toDoList.filter((_, j) => toDoIndex !== j);
                    displayToDo()
                } else {
                    alert('no se pudo eliminar tu ToDo')
                }
            })

        })
    }
    //se agrega el boton de editar y se le da funcionalidad
    const buttonsEdit = document.querySelectorAll('.button-edit-to-do')!;
    for (let i = 0; buttonsEdit.length > i; i++) {
        buttonsEdit[i].addEventListener('click', (e) => {
            const toDoIndex = parseInt((e.target! as HTMLElement).dataset.itemList!)
            toDoList[toDoIndex].isEditing = true

            upDateToDo(toDoList[toDoIndex], (error?: Error) => {
                if (!error) {
                    displayToDo()
                } else {
                    alert('no se pudo actualizar')
                }
            })

        })
    }

    //funcionalidad del boton cancelar al momento de editar

    const buttonsEditCancel = document.querySelectorAll('.button-edit-cancel-to-do')!;
    for (let i = 0; buttonsEditCancel.length > i; i++) {
        buttonsEditCancel[i].addEventListener('click', (e) => {
            const toDoIndex = parseInt((e.target! as HTMLElement).dataset.itemList!)
            toDoList[toDoIndex].isEditing = false
            upDateToDo(toDoList[toDoIndex], (error?: Error) => {
                if (!error) {
                    displayToDo()
                } else {
                    alert('no se pudo cancelar')
                }
            })
        })
    }

    //le damos funcionalidad al boton save..
    const buttonsSave = document.querySelectorAll('.button-edit-save-to-do')!;
    for (let i = 0; buttonsSave.length > i; i++) {
        buttonsSave[i].addEventListener('click', (e) => {
            const toDoIndex = parseInt((e.target! as HTMLElement).dataset.itemList!)
            const inputValue=(document.getElementById(`input-to-do-edit-${toDoIndex}`)! as HTMLInputElement).value
              
            if (!inputValue) {
                document.querySelector(`[data-item-error="${toDoIndex}"]`)?.classList.add('visible')
            } else {
                toDoList[toDoIndex].value= inputValue
                toDoList[toDoIndex].isEditing = false
                //actualizamos la DB para el boton guardar al momento de editar 
                upDateToDo(toDoList[toDoIndex], (error?: Error) => {
                    if (!error) {
                        displayToDo()
                    } else {
                        alert('no se pudo editar tu ToDo')
                    }
                })

            }

        })
    }
    //funcionalidad al boton complete al momento de estar en modo normal, para que cuando se le de click se vaya a modo complete
    const buttonsComplete = document.querySelectorAll('.button-complete-to-do')!;
    for (let i = 0; buttonsComplete.length > i; i++) {
        buttonsComplete[i].addEventListener('click', (e) => {
            const toDoIndex = parseInt((e.target! as HTMLElement).dataset.itemList!)
            toDoList[toDoIndex].isComplete = true
            upDateToDo(toDoList[toDoIndex], (error?: Error) => {
                if (!error) {
                    displayToDo()
                } else {
                    alert('se completo tu Todo')
                }
            })
        })
    }

    //funcionalidad al boton no complete modo complete , para estar en modo normal
    const buttonsNoComplete = document.querySelectorAll('.button-no-complete-to-do')!;
    for (let i = 0; buttonsNoComplete.length > i; i++) {
        buttonsNoComplete[i].addEventListener('click', (e) => {
            const toDoIndex = parseInt((e.target! as HTMLElement).dataset.itemList!)
            toDoList[toDoIndex].isComplete = false
            upDateToDo(toDoList[toDoIndex], (error?: Error) => {
                if (!error) {
                    displayToDo()
                } else {
                    alert('no se completo tu Todo')
                }
            })

        })
    }

}

const inputToDo = (document.getElementById('input-to-do')! as HTMLInputElement);

inputToDo.addEventListener('input', () => {
    newToDo = inputToDo.value
    document.querySelector('.error-mesage-input-new-toDo')?.classList.remove('visible')
})

///mandamos a la DB el nuevo ToDO
document.getElementById('button-add-to-do')!.addEventListener('click', () => {
    if (!newToDo) {
        document.querySelector('.error-mesage-input-new-toDo')?.classList.add('visible')
    } else {
        const saveNewToDo = {
            value: newToDo,
            isEditing: false,
            isComplete: false,
            id: uuidv4(),
            creationDate: new Date()
        };
        createToDo(saveNewToDo, (error?: Error) => {
            if (!error) {
                toDoList.push(saveNewToDo)
                displayToDo();
                newToDo = '';
                inputToDo.value = '';
            } else {
                alert('no es posible guardar tu ToDo');
            }
        })

    }
})
// cargamos la lista de toDos
loadToDo((error?: Error, todos?: ToDo[]) => {
    if (!error) {
        toDoList = todos!
        displayToDo();
    } else {
        alert('no se cargo');
    }
})

