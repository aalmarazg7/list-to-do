import type { ToDo, ErrorResult } from '../types';

//definimos nuestra variable de la bd
const storeName = 'ToDo';
const dbName = 'DB-list-to-do';
let db: IDBDatabase | undefined;
let bounceInterval: number;

//se abre la conexión a la DB
const request = indexedDB.open(dbName);
request.onsuccess = () => {
    db = request.result
    console.log("openDb DONE");
};
request.onerror = (evt) => {
    // @ts-ignore
    console.error("openDb:", evt.target?.errorCode);
};

//crear la DB
request.onupgradeneeded = (evt) => {
    console.log("openDb.onupgradeneeded");
    //aqui se define la estructura de la DB  
    // @ts-ignore
    const toDoStore = evt.target?.result.createObjectStore(storeName, { keyPath: 'id' });
}
//con esta funcion cargamos los ToDo de la DB, usaremos bounce request para ver si la DB ya esta lista, o nos 
//esperamos a ver que este lista
export const loadToDo = (loaded: (error?: Error, todos?: ToDo[]) => void) => {
    bounceInterval = setInterval(() => {
        //revisamos que la DB este lista
        if (db) {
            //cancelamos el intervalo
            clearInterval(bounceInterval);
            const txStore = db.transaction(storeName, 'readwrite')?.objectStore(storeName)
            const result = txStore?.getAll()
            //@ts-ignore
            result.onsuccess = (evt) => {
                //@ts-ignore
                loaded(undefined, evt.target.result)
            }
            //@ts-ignore
            result.onerror = (evt) => {
                //@ts-ignore
                loaded(evt.target.error)
            }

        }
    },100)
}


//esta funcion no regresa nada (void) pero si recibe parametros, esta funcion la usamos para crear
export const createToDo = (newToDo: ToDo, created: ErrorResult) => {
    const txStore = db?.transaction(storeName, 'readwrite')?.objectStore(storeName)
    const result = txStore?.add(newToDo)
    //@ts-ignore
    result.onsuccess = () => {
        created()
    }
    //@ts-ignore
    result.onerror = (evt) => {
        //@ts-ignore
        created(evt.target.error)

    }
}
//actualizamos un ToDo en la DB
export const upDateToDo =(toDo: ToDo, upDated: ErrorResult)=>{
    const txStore = db?.transaction(storeName, 'readwrite')?.objectStore(storeName)
    const result = txStore?.put(toDo)
    //@ts-ignore
    result.onsuccess = () => {
        upDated()
    }
    //@ts-ignore
    result.onerror = (evt) => {
        //@ts-ignore
        upDated(evt.target.error)

    }
}

//borramos un ToDo de la DB
export const deleteToDo = (toDo: ToDo, deleted: ErrorResult) => {
    //necesitamos un objet store y lo mandamos a llamar, creando una transacción
    const txStore = db?.transaction(storeName, 'readwrite')?.objectStore(storeName)
    //definimos una variable para guardar lo que nos regrese
    const result =txStore?.delete(toDo.id)
    //hacemos el callback para la db
    //@ts-ignore
    result.onsuccess = ()=>{
        deleted()
    }
    //@ts-ignore
    result.onerror= (evt) =>{
        //@ts-ignore
        deleted(evt.target.error)
    }

}
