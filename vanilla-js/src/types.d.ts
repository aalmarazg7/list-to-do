export type ToDo = {
    value: string;
    isEditing: boolean;
    isComplete:boolean;
    id:string;
    creationDate:Date
}

export type ErrorResult = (error?:Error)=>void;